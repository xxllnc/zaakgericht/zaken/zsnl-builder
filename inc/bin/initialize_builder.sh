#!/usr/bin/env bash

## Create a builder instance and select it
BUILDER=$( docker buildx create --use --platform linux/amd64 --name mintlab-ci )

if [ -z "$XXLLNC_CI_NO_SIDECAR" ]; then
    # arm64 sidecar box enabled
    REGION="${XXLLNC_CI_REGION:-eu-central-1}"
    ASG_ARN_FILTER="${XXLLNC_CI_ARN_FILTER:-xxllnc-ci-arm}"
    ASG_TEMPLATE="${XXLLNC_CI_ASG_TEMPLATE:-xxllnc-ci-arm-%s.dmz.zsys.nl}"

    function get_boxes {
        aws autoscaling describe-auto-scaling-groups --region "$REGION" \
        | jq -r '.AutoScalingGroups[]
                | select(.AutoScalingGroupARN | contains("'"${ASG_ARN_FILTER}"'")).Instances[]
                | select(.LifecycleState      | contains("InService")).InstanceId' \
        | xargs printf "${ASG_TEMPLATE}\\n"
    }

    ## Loop over all boxes and add to builder instance
    for box in $( get_boxes ); do
        echo "Adding box '$box' to builder '$BUILDER'"
        docker buildx create --append --driver docker-container --name "$BUILDER" --platform linux/arm64 --node "$box" "$box:2375"
    done
fi

## Bootstrap our cluster
echo "Bootstrapping buildx on all nodes"
docker buildx inspect --bootstrap
