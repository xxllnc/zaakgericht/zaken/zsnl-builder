FROM docker
COPY --from=docker/buildx-bin:latest /buildx /usr/libexec/docker/cli-plugins/docker-buildx

LABEL maintainer ops@mintlab.nl
LABEL description "Builds Zaaksysteem images"

# copy the initialize script
COPY inc/bin/initialize_builder.sh /usr/local/bin/
RUN ln -s /usr/local/bin/initialize_builder.sh /init.sh

# Copy the policy file Skopeo uses
COPY policy.json /etc/containers/

# docker:latest (or one of its parent images) no longer installs curl, we need
# curl to poke the k8s deploy stage
RUN apk --no-cache --update add bash curl python3 py3-pip jq \
 && apk --no-cache --update --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community add skopeo \
 && pip install awscli
